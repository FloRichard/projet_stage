package io.swagger.api;

import Tools.DataBase;
import io.swagger.model.Dossier;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-18T17:18:37.321Z")

@Controller
public class DossiersApiController implements DossiersApi {

    private static final Logger log = LoggerFactory.getLogger(DossiersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public DossiersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Dossier> findAfileByID(@ApiParam(value = "Numéro de dossier",required=true) @PathVariable("dos_num") String dosNum) {
        String accept = request.getHeader("Accept");
        accept += "application/json";
        if (accept != null && accept.contains("application/json")) {
            try {
                String json = DataBase.getDossier(dosNum);
                System.out.println(json);
                return new ResponseEntity<Dossier>(objectMapper.readValue(json, Dossier.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Dossier>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (ApiException a){
                return new ResponseEntity<Dossier>(HttpStatus.valueOf(404));
            }
        }
        return new ResponseEntity<Dossier>(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    public ResponseEntity<List<Dossier>> findFilesByDoctor(@ApiParam(value = "Numéro d'un médecin",required=true) @PathVariable("med_num") String medNum) {
        String accept = request.getHeader("Accept");
        accept += "application/json";
        if (accept != null && accept.contains("application/json")) {
            try {
                String json = DataBase.getDossiers(medNum);
                return new ResponseEntity<List<Dossier>>(objectMapper.readValue(json, List.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Dossier>>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }

        return new ResponseEntity<List<Dossier>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
