package io.swagger.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

import jdk.nashorn.internal.objects.annotations.Constructor;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Dossier
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-18T17:18:37.321Z")

public class Dossier   {
  @JsonProperty("dos_num")
  private String dosNum = null;

  @JsonProperty("medecinID")
  private String medecinID = null;

  @JsonProperty("nomMedecin")
  private String nomMedecin = null;

  @JsonProperty("prenomMedecin")
  private String prenomMedecin = null;

  @JsonProperty("patientID")
  private String patientID = null;

  @JsonProperty("nomPatient")
  private String nomPatient = null;

  @JsonProperty("prenomPatient")
  private String prenomPatient = null;

  @JsonProperty("historiqueMaladie")
  @Valid
  private List<String> historiqueMaladie = new ArrayList<>();

  @JsonProperty("listeMedicament")
  @Valid
  private List<String> listeMedicament = new ArrayList<>();

 /* public Dossier(ResultSet data,ResultSet medicaments,ResultSet maladie){
    try {
      while(data.next()) {
        dosNum(data.getString(1));
        medecinID((data.getString(2)));
        nomMedecin(data.getString(3));
        prenomMedecin(data.getString(4));
        patientID(data.getString(5));
        nomPatient(data.getString(6));
        prenomPatient(data.getString(7));
      }
      while (medicaments.next()){

        addListeMedicamentItem(medicaments.getString(1));
      }
      while(maladie.next()){
        this.addHistoriqueMaladieItem(maladie.getString(1));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }*/


  public Dossier dosNum(String dosNum) {
    this.dosNum = dosNum;
    return this;
  }

  /**
   * Get dosNum
   * @return dosNum
  **/
  @ApiModelProperty(value = "")


  public String getDosNum() {
    return dosNum;
  }

  public void setDosNum(String  dosNum) {
    this.dosNum = dosNum;
  }

  public Dossier medecinID(String medecinID) {
    this.medecinID = medecinID;
    return this;
  }

  /**
   * Numéro CIS du médecin
   * @return medecinID
  **/
  @ApiModelProperty(value = "Numéro CIS du médecin")


  public String getMedecinID() {
    return medecinID;
  }

  public void setMedecinID(String medecinID) {
    this.medecinID = medecinID;
  }

  public Dossier nomMedecin(String nomMedecin) {
    this.nomMedecin = nomMedecin;
    return this;
  }

  /**
   * Nom du médecin
   * @return nomMedecin
  **/
  @ApiModelProperty(value = "Nom du médecin")


  public String getNomMedecin() {
    return nomMedecin;
  }

  public void setNomMedecin(String nomMedecin) {
    this.nomMedecin = nomMedecin;
  }

  public Dossier prenomMedecin(String prenomMedecin) {
    this.prenomMedecin = prenomMedecin;
    return this;
  }

  /**
   * Prénom du médecin
   * @return prenomMedecin
  **/
  @ApiModelProperty(value = "Prénom du médecin")


  public String getPrenomMedecin() {
    return prenomMedecin;
  }

  public void setPrenomMedecin(String prenomMedecin) {
    this.prenomMedecin = prenomMedecin;
  }

  public Dossier patientID(String patientID) {
    this.patientID = patientID;
    return this;
  }

  /**
   * Numéro du patient
   * @return patientID
  **/
  @ApiModelProperty(value = "Numéro du patient")


  public String getPatientID() {
    return patientID;
  }

  public void setPatientID(String patientID) {
    this.patientID = patientID;
  }

  public Dossier nomPatient(String nomPatient) {
    this.nomPatient = nomPatient;
    return this;
  }

  /**
   * Nom du patient
   * @return nomPatient
  **/
  @ApiModelProperty(value = "Nom du patient")


  public String getNomPatient() {
    return nomPatient;
  }

  public void setNomPatient(String nomPatient) {
    this.nomPatient = nomPatient;
  }

  public Dossier prenomPatient(String prenomPatient) {
    this.prenomPatient = prenomPatient;
    return this;
  }

  /**
   * Prénom du patient
   * @return prenomPatient
  **/
  @ApiModelProperty(value = "Prénom du patient")


  public String getPrenomPatient() {
    return prenomPatient;
  }

  public void setPrenomPatient(String prenomPatient) {
    this.prenomPatient = prenomPatient;
  }

  public Dossier historiqueMaladie(List<String> historiqueMaladie) {
    this.historiqueMaladie = historiqueMaladie;
    return this;
  }

  public Dossier addHistoriqueMaladieItem(String historiqueMaladieItem) {
    if (this.historiqueMaladie == null) {
      this.historiqueMaladie = new ArrayList<String>();
    }
    this.historiqueMaladie.add(historiqueMaladieItem);
    return this;
  }

  /**
   * Get historiqueMaladie
   * @return historiqueMaladie
  **/
  @ApiModelProperty(value = "")


  public List<String> getHistoriqueMaladie() {
    return historiqueMaladie;
  }

  public void setHistoriqueMaladie(List<String> historiqueMaladie) {
    this.historiqueMaladie = historiqueMaladie;
  }

  public Dossier listeMedicament(List<String> listeMedicament) {
    this.listeMedicament = listeMedicament;
    return this;
  }

  public Dossier addListeMedicamentItem(String listeMedicamentItem) {
    if (this.listeMedicament == null) {
      this.listeMedicament = new ArrayList<String>();
    }
    this.listeMedicament.add(listeMedicamentItem);
    return this;
  }

  /**
   * Get listeMedicament
   * @return listeMedicament
  **/
  @ApiModelProperty(value = "")


  public List<String> getListeMedicament() {
    return listeMedicament;
  }

  public void setListeMedicament(List<String> listeMedicament) {
    this.listeMedicament = listeMedicament;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dossier dossier = (Dossier) o;
    return Objects.equals(this.dosNum, dossier.dosNum) &&
        Objects.equals(this.medecinID, dossier.medecinID) &&
        Objects.equals(this.nomMedecin, dossier.nomMedecin) &&
        Objects.equals(this.prenomMedecin, dossier.prenomMedecin) &&
        Objects.equals(this.patientID, dossier.patientID) &&
        Objects.equals(this.nomPatient, dossier.nomPatient) &&
        Objects.equals(this.prenomPatient, dossier.prenomPatient) &&
        Objects.equals(this.historiqueMaladie, dossier.historiqueMaladie) &&
        Objects.equals(this.listeMedicament, dossier.listeMedicament);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dosNum, medecinID, nomMedecin, prenomMedecin, patientID, nomPatient, prenomPatient, historiqueMaladie, listeMedicament);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Dossier {\n");
    
    sb.append("    dosNum: ").append(toIndentedString(dosNum)).append("\n");
    sb.append("    medecinID: ").append(toIndentedString(medecinID)).append("\n");
    sb.append("    nomMedecin: ").append(toIndentedString(nomMedecin)).append("\n");
    sb.append("    prenomMedecin: ").append(toIndentedString(prenomMedecin)).append("\n");
    sb.append("    patientID: ").append(toIndentedString(patientID)).append("\n");
    sb.append("    nomPatient: ").append(toIndentedString(nomPatient)).append("\n");
    sb.append("    prenomPatient: ").append(toIndentedString(prenomPatient)).append("\n");
    sb.append("    historiqueMaladie: ").append(toIndentedString(historiqueMaladie)).append("\n");
    sb.append("    listeMedicament: ").append(toIndentedString(listeMedicament)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

