package Tools;
import io.swagger.api.ApiException;
import io.swagger.api.ApiResponseMessage;
import springfox.documentation.spring.web.json.Json;

import java.sql.*;
import java.util.ArrayList;

public class DataBase {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/sante";
    static final String USER = "root";
    static final String PASS = "";
    static String fileInfoQuery  = "select distinct dos_num,med_rpps_num,med_nom,med_prenom,pat_identifiant,pat_nom,pat_prenom " +
            "FROM san_dossier JOIN san_medecin USING(med_rpps_num) JOIN san_patient USING(pat_identifiant) WHERE dos_num= '";
    static String medicamentQuery = "select medi_libelle from san_medicament join san_prescrire USING (medi_cis) where dos_num = ";
    static String maladiesQuery = "select mal_libelle from san_maladie join san_traiter USING (MAL_CIM) where dos_num = ";

    /**
     * Méthode permettant de se connecter à la base de données
     * @return la connexion
     */
    public static Connection connection(){
        Connection conn = null;
        try {
            Class.forName(JDBC_DRIVER);
            System.out.println("Connexion à la base...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            System.out.println("Connexion réussie");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static ArrayList<String> executeAQuery(String query)  {
        ArrayList<String> liste = new ArrayList<>();
        try {
            Class.forName(JDBC_DRIVER);
            System.out.println("Connexion à la base...");
            Connection conn = DriverManager.getConnection(DB_URL,USER,PASS);
            System.out.println("Connexion réussie");
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while(resultSet.next()) {
                for(int i = 1; i<=metaData.getColumnCount();i++) {
                    liste.add(resultSet.getString(i));
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return liste;
    }
    /**
     * Méthode permettant d'éxecuter une requête prépapée
     * @param query la requête
     * @param parameters les paramètres de la requête
     * @param conn la connexion
     * @return le ResultSet de la requête
     */
    public  static ResultSet executePreparedQuery(String query, ArrayList<String> parameters,Connection conn){
        ResultSet resultSetMalNums = null;
        try {
            PreparedStatement statementMalNums = null;
            statementMalNums = conn.prepareStatement(query);
            for(int i = 0; i<parameters.size();i++){
                statementMalNums.setString(i+1,parameters.get(i));
            }
            resultSetMalNums = statementMalNums.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSetMalNums;
    }
    /**
     * Créer la chaîne JSON representant un dossier
     * @param dos_num le numéro de dossier souhaité
     * @return la chaîne en notation JSON
     */
    public static String getDossier(String dos_num) throws ApiException {
        Connection con = connection();
        StringBuilder json =  new StringBuilder();
        try {
            String queryFile = "select distinct dos_num,med_rpps_num,med_nom,med_prenom,pat_identifiant,pat_nom,pat_prenom FROM san_dossier JOIN san_medecin USING(med_rpps_num) JOIN san_patient USING(pat_identifiant) WHERE dos_num= ?";
            ArrayList<String> param =  new ArrayList<>();
            param.add(dos_num);
            ResultSet resultSetQueryFile = executePreparedQuery(queryFile,param,con);
            ResultSetMetaData resultSetMetaData = resultSetQueryFile.getMetaData();
            ArrayList<String> info = new ArrayList<>();
            while (resultSetQueryFile.next()){
                for(int i = 1; i<=resultSetMetaData.getColumnCount();i++) {
                    info.add(resultSetQueryFile.getString(i));
                }
            }
            ArrayList<String> listeMedicaments = new ArrayList<>();
            String queryForM = "select medi_libelle from san_medicament join san_prescrire USING (medi_cis) where dos_num = ?";
            ResultSet resultSetForQueryForM =executePreparedQuery(queryForM,param,con);
            while (resultSetForQueryForM.next()){
                listeMedicaments.add(resultSetForQueryForM.getString(1));
            }
            ArrayList<String> listeMaladies = new ArrayList<>();
            String queryForDiseases = " select mal_libelle from san_maladie join san_traiter USING (MAL_CIM) where dos_num = ? ";
            ResultSet resultSetForQueryForDiseases = executePreparedQuery(queryForDiseases,param,con);
            while (resultSetForQueryForDiseases.next()){
                listeMaladies.add(resultSetForQueryForDiseases.getString(1));
            }

            json.append("{ \"dos_num\" : \"" + info.get(0) + "\",  \"medecinID\" : \"" + info.get(1) + "\", \"nomMedecin\" : \"" + info.get(2) + "\",  \"prenomMedecin\" : \"" + info.get(3) + "\", \"patientID\" : \"" + info.get(4) + "\",  \"prenomPatient\" : \"" + info.get(6) + "\",  \"nomPatient\" : \"" + info.get(5) + "\",");
            json.append(" \"listeMedicament\" : [ ");
            addListe(json, listeMedicaments);
            json.append("],\"historiqueMaladie\" : [  ");
            addListe(json, listeMaladies);
            json.append("]}");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return json.toString();

    }

    public static String getDossiers(String med_num) throws ApiException {
        Connection conn = connection();
        StringBuilder json = new StringBuilder();
        ArrayList<String> dos_nums = new ArrayList<>();
        String queryForDosNum = "SELECT DISTINCT dos_num FROM san_dossier JOIN san_medecin USING(med_rpps_num) JOIN san_patient USING(pat_identifiant) WHERE MED_RPPS_NUM= ?";
        ArrayList<String> param = new ArrayList<>();
        param.add(med_num);
        ResultSet resultSetForDosNum = executePreparedQuery(queryForDosNum,param,conn);
        try {
            while (resultSetForDosNum.next()){
                dos_nums.add(resultSetForDosNum.getString(1));
            }
            json.append("[ ");
            for(int i=0 ; i < dos_nums.size();i++){
                json.append(getDossier(dos_nums.get(i)));
                if(i != dos_nums.size() -1){
                    json.append(",");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        json.append("]");
        return json.toString();
    }
    /**
     * Méthode permettant d'ajouter une liste de données à la chaine
     * @param json le stringBuilder
     * @param liste la liste à ajouter
     */
    private static void addListe(StringBuilder json, ArrayList<String> liste) {
        for (int i = 0; i < liste.size(); i++) {
            json.append("\""+liste.get(i)+"\"");
            if(i!=(liste.size()-1))
                json.append(",");
        }
    }
}
