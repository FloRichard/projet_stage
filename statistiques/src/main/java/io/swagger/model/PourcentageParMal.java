package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PourcentageParMal
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-19T18:52:26.976Z")

public class PourcentageParMal   {
  @JsonProperty("nbPatients")
  @Valid
  private List<Integer> nbPatients = null;

  @JsonProperty("maladies")
  @Valid
  private List<String> maladies = null;

  public PourcentageParMal nbPatients(List<Integer> nbPatients) {
    this.nbPatients = nbPatients;
    return this;
  }

  public PourcentageParMal addNbPatientsItem(Integer nbPatientsItem) {
    if (this.nbPatients == null) {
      this.nbPatients = new ArrayList<Integer>();
    }
    this.nbPatients.add(nbPatientsItem);
    return this;
  }

  /**
   * Get nbPatients
   * @return nbPatients
  **/
  @ApiModelProperty(value = "")


  public List<Integer> getNbPatients() {
    return nbPatients;
  }

  public void setNbPatients(List<Integer> nbPatients) {
    this.nbPatients = nbPatients;
  }

  public PourcentageParMal maladies(List<String> maladies) {
    this.maladies = maladies;
    return this;
  }

  public PourcentageParMal addMaladiesItem(String maladiesItem) {
    if (this.maladies == null) {
      this.maladies = new ArrayList<String>();
    }
    this.maladies.add(maladiesItem);
    return this;
  }

  /**
   * Get maladies
   * @return maladies
  **/
  @ApiModelProperty(value = "")


  public List<String> getMaladies() {
    return maladies;
  }

  public void setMaladies(List<String> maladies) {
    this.maladies = maladies;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PourcentageParMal pourcentageParMal = (PourcentageParMal) o;
    return Objects.equals(this.nbPatients, pourcentageParMal.nbPatients) &&
        Objects.equals(this.maladies, pourcentageParMal.maladies);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nbPatients, maladies);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PourcentageParMal {\n");
    
    sb.append("    nbPatients: ").append(toIndentedString(nbPatients)).append("\n");
    sb.append("    maladies: ").append(toIndentedString(maladies)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

