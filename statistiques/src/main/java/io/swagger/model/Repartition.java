package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Repartition
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-19T18:52:26.976Z")

public class Repartition   {
  @JsonProperty("maladie")
  private String maladie = null;

  @JsonProperty("inf20")
  private Integer inf20 = null;

  @JsonProperty("inf40")
  private Integer inf40 = null;

  @JsonProperty("inf60")
  private Integer inf60 = null;

  @JsonProperty("inf80")
  private Integer inf80 = null;

  @JsonProperty("inf100")
  private Integer inf100 = null;

  public Repartition maladie(String maladie) {
    this.maladie = maladie;
    return this;
  }

  /**
   * Get maladie
   * @return maladie
  **/
  @ApiModelProperty(value = "")


  public String getMaladie() {
    return maladie;
  }

  public void setMaladie(String maladie) {
    this.maladie = maladie;
  }

  public Repartition inf20(Integer inf20) {
    this.inf20 = inf20;
    return this;
  }

  /**
   * Get inf20
   * @return inf20
  **/
  @ApiModelProperty(value = "")


  public Integer getInf20() {
    return inf20;
  }

  public void setInf20(Integer inf20) {
    this.inf20 = inf20;
  }

  public Repartition inf40(Integer inf40) {
    this.inf40 = inf40;
    return this;
  }

  /**
   * Get inf40
   * @return inf40
  **/
  @ApiModelProperty(value = "")


  public Integer getInf40() {
    return inf40;
  }

  public void setInf40(Integer inf40) {
    this.inf40 = inf40;
  }

  public Repartition inf60(Integer inf60) {
    this.inf60 = inf60;
    return this;
  }

  /**
   * Get inf60
   * @return inf60
  **/
  @ApiModelProperty(value = "")


  public Integer getInf60() {
    return inf60;
  }

  public void setInf60(Integer inf60) {
    this.inf60 = inf60;
  }

  public Repartition inf80(Integer inf80) {
    this.inf80 = inf80;
    return this;
  }

  /**
   * Get inf80
   * @return inf80
  **/
  @ApiModelProperty(value = "")


  public Integer getInf80() {
    return inf80;
  }

  public void setInf80(Integer inf80) {
    this.inf80 = inf80;
  }

  public Repartition inf100(Integer inf100) {
    this.inf100 = inf100;
    return this;
  }

  /**
   * Get inf100
   * @return inf100
  **/
  @ApiModelProperty(value = "")


  public Integer getInf100() {
    return inf100;
  }

  public void setInf100(Integer inf100) {
    this.inf100 = inf100;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Repartition repartition = (Repartition) o;
    return Objects.equals(this.maladie, repartition.maladie) &&
        Objects.equals(this.inf20, repartition.inf20) &&
        Objects.equals(this.inf40, repartition.inf40) &&
        Objects.equals(this.inf60, repartition.inf60) &&
        Objects.equals(this.inf80, repartition.inf80) &&
        Objects.equals(this.inf100, repartition.inf100);
  }

  @Override
  public int hashCode() {
    return Objects.hash(maladie, inf20, inf40, inf60, inf80, inf100);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Repartition {\n");
    
    sb.append("    maladie: ").append(toIndentedString(maladie)).append("\n");
    sb.append("    inf20: ").append(toIndentedString(inf20)).append("\n");
    sb.append("    inf40: ").append(toIndentedString(inf40)).append("\n");
    sb.append("    inf60: ").append(toIndentedString(inf60)).append("\n");
    sb.append("    inf80: ").append(toIndentedString(inf80)).append("\n");
    sb.append("    inf100: ").append(toIndentedString(inf100)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

