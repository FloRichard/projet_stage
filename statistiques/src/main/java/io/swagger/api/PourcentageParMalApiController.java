package io.swagger.api;

import Tools.DataBase;
import io.swagger.model.PourcentageParMal;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-19T18:52:26.976Z")

@Controller
public class PourcentageParMalApiController implements PourcentageParMalApi {

    private static final Logger log = LoggerFactory.getLogger(PourcentageParMalApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public PourcentageParMalApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<PourcentageParMal> getPourcentage(@NotNull @ApiParam(value = "Numéro du médecin et numéro de la maladie", required = true) @Valid @RequestParam(value = "med_num", required = true) String medNum,@NotNull @ApiParam(value = "Numéro de la maladie", required = true) @Valid @RequestParam(value = "mal_num", required = true) String malNum) {
        String accept = request.getHeader("Accept");
        accept += "application/json";
        if (accept != null && accept.contains("application/json")) {
            try {
                String json = DataBase.getPourcentageParMal(medNum,malNum);
                return new ResponseEntity<PourcentageParMal>(objectMapper.readValue(json, PourcentageParMal.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<PourcentageParMal>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<PourcentageParMal>(HttpStatus.valueOf(404));
    }

}
