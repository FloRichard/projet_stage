package Tools;
import io.swagger.api.ApiException;
import io.swagger.api.ApiResponseMessage;
import springfox.documentation.spring.web.json.Json;

import java.sql.*;
import java.util.ArrayList;

public class DataBase {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/sante";
    static final String USER = "root";
    static final String PASS = "";

    /**
     * Méthode permettant de se connecter à la base de données
     * @return la connexion
     */
    public static Connection connection(){
        Connection conn = null;
        try {
            Class.forName(JDBC_DRIVER);
            System.out.println("Connexion à la base...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            System.out.println("Connexion réussie");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * Méthode permettant d'éxecuter une requête simple
     * @param query la requête
     * @return résultat de la requête sous forme d'une liste
     */
    public static ArrayList<String> executeAQuery(String query)  {
        ArrayList<String> liste = new ArrayList<>();
        try {
            Connection conn = connection();
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while(resultSet.next()) {
                for(int i = 1; i<=metaData.getColumnCount();i++) {
                    liste.add(resultSet.getString(i));
                }
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return liste;
    }

    /**
     * Méthode permettant d'éxecuter une requête prépapée
     * @param query la requête
     * @param parameters les paramètres de la requête
     * @param conn la connexion
     * @return le ResultSet de la requête
     */
    public  static ResultSet executePreparedQuery(String query, ArrayList<String> parameters,Connection conn){
        ResultSet resultSetMalNums = null;
        try {
            PreparedStatement statementMalNums = null;
            statementMalNums = conn.prepareStatement(query);
            for(int i = 0; i<parameters.size();i++){
                statementMalNums.setString(i+1,parameters.get(i));
            }
            resultSetMalNums = statementMalNums.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSetMalNums;
    }

    /**
     * Méthode permettant d'ajouter une liste de données à la chaîne
     * @param json le stringBuilder
     * @param liste la liste à ajouter
     */
    private static void addListe(StringBuilder json, ArrayList<String> liste) {
        for (int i = 0; i < liste.size(); i++) {
            json.append("\""+liste.get(i)+"\"");
            if(i!=(liste.size()-1))
                json.append(",");
        }
    }

    /**
     * Méthode permettant de récupérer les données nécessaires pour les pourcentages de patients par maladie d'un médecin
     * @param med_num le numéro du médecin
     * @param mal_num
     * @return la chaîne au format JSON
     */
    public static String getPourcentageParMal(String med_num,String mal_num){
        StringBuilder json = new StringBuilder();
        try {
            Connection conn = connection();
            String queryMalNums = "select distinct MAL_CIM from san_traiter join san_dossier USING (DOS_NUM) where med_rpps_num = ?";
            ArrayList<String> parametersForQueryMalNums = new ArrayList<>();
            parametersForQueryMalNums.add(med_num);
            ResultSet resultSetMalNums = executePreparedQuery(queryMalNums,parametersForQueryMalNums,conn);
            ArrayList<String> malNums = new ArrayList<>();
            while (resultSetMalNums.next()){
                malNums.add(resultSetMalNums.getString(1));
            }
            ArrayList<String> nbPatients = new ArrayList<>();
            ArrayList<String> maladies = new ArrayList<>();
            for(int i = 0;i < malNums.size();i++) {
                String queryPercentage = "select count(*), MAL_LIBELLE from san_traiter join san_dossier USING (DOS_NUM) join san_patient USING (pat_identifiant) JOIN san_maladie USING (MAL_CIM) where MAL_CIM = ? and MED_RPPS_NUM = ?";
                ArrayList<String> paramsForQueryPercentage = new ArrayList<>();
                paramsForQueryPercentage.add(malNums.get(i));
                paramsForQueryPercentage.add(med_num);
                ResultSet resultSet = executePreparedQuery(queryPercentage,paramsForQueryPercentage,conn);
                ResultSetMetaData metaData = resultSet.getMetaData();

                while (resultSet.next()) {
                    nbPatients.add(resultSet.getString(1));
                    maladies.add(resultSet.getString(2));
                }
            }
            json.append("{ \"nbPatients\" : [");
            addListe(json,nbPatients);
            json.append(" ], \"maladies\" : [");
            addListe(json,maladies);
            json.append("]}");
            conn.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * Méthode d'avoir une répartion des patients par tranches pour une maladie
     * @param med_num numéro RPPS du médecin
     * @param mal_num numéro CIM de la maladie
     * @return une chaîne au format JSON
     */
    public static String getRepartion(String med_num,String mal_num) throws ApiException{
        ArrayList<String> tranchesAge = new ArrayList<>();
        tranchesAge.add("0");
        tranchesAge.add("20");
        tranchesAge.add("40");
        tranchesAge.add("60");
        tranchesAge.add("80");
        tranchesAge.add("100");
        StringBuilder json = new StringBuilder();
        Connection conn = connection();
        String queryForRepartion = "select count(*),mal_libelle from san_patient join san_dossier USING (PAT_IDENTIFIANT) JOIN san_traiter USING (dos_num) join san_maladie USING (MAL_CIM) where DATEDIFF(CURRENT_DATE,PAT_DATE_NAISSANCE)/365 BETWEEN ? and ? and med_rpps_num = ? and mal_cim = ?";
        String repartion[] = new String[5];
        String maladie = null;
        for(String s:repartion){
            s= "0";
        }
        for(int j = 0 ; j < tranchesAge.size()-1;j++ ) {
            ArrayList<String> paramsForRepartition = new ArrayList<>();
            paramsForRepartition.add(tranchesAge.get(j));
            paramsForRepartition.add(tranchesAge.get(j + 1));
            paramsForRepartition.add(med_num);
            paramsForRepartition.add(mal_num);
            ResultSet resultSetRepartition = executePreparedQuery(queryForRepartion, paramsForRepartition, conn);
            try {
                while (resultSetRepartition.next()) {
                    if(resultSetRepartition.getString(1) != null);
                        repartion[j] = resultSetRepartition.getString(1);
                    maladie = resultSetRepartition.getString(2);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        json.append("{ \"maladie\" : \"" + maladie + "\", \"inf20\" : \"" + repartion[0] + "\"" +
                ", \"inf40\" : \"" + repartion[1] + "\" , \"inf60\" : \"" + repartion[2] + "\"" +
                ", \"inf80\" : \"" + repartion[3] + "\" , \"inf100\" : \"" + repartion[4] + "\"}"
        );
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return json.toString();
    }
}
